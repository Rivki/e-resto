<div class="col-xl-12 order-xl-1">
    <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0"><?= $judul ?></h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Meja</label>
                                <input type="text" name="nama" id="input-username" class="form-control form-control-alternative" placeholder="Meja">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Status</label>
                                <select name="status" class="form-control form-control-alternative" id="">
                                    <option value="free">Free</option>
                                    <option value="used">Used</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?= base_url() ?>meja" class="btn btn-danger">Kembali</a>
                <input type="submit" class="btn btn-success float-right" name="submit" value="Simpan">
            </form>
        </div>
    </div>
</div>