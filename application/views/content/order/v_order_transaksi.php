<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <h3 class="mb-0"><?= $judul ?></h3>
            </div>
            <div class="table-responsive">
                <table id="dataTable" class="table align-items-center table-flush">
                    <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th class="hidden">ID</th>
                        <th>Nama</th>
                        <th>Qty</th>
                        <th>Harga</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($trx as $t) {
                        ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td class="hidden"><?= $t->id ?></td>
                            <td><?= $t->nama ?></td>
                            <td><?= $t->qty ?></td>
                            <td><?= rupiah($t->harga) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <form action="" method="post">
                    <div class="form-group">
                        <label class="offset-sm-7" for="">Total Harga</label>
                        <input type="text" id="total-harga" class="col-sm-3 left--5 float-sm-right form-control" value="<?=$order->total_harga?>" readonly>
                    </div>
                    <div class="form-group">
                        <label class="offset-sm-7" for="">Pembayaran</label>
                        <input type="text" id="bayar" class="col-sm-3 left--5 float-sm-right form-control" value="0">
                    </div>
                    <div class="form-group">
                        <label class="offset-sm-7" for="">Kembalian</label>
                        <input type="text" id="kembalian" class="col-sm-3 left--5 float-sm-right form-control" value="0" readonly>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="bayar" class="btn btn-lg btn-success float-right left--5" value="Proses">
                        <input type="hidden" name="id_meja" value="<?=$order->meja_id?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".hidden").hide();
        function hitungKembalian(){
            let total = $('#total-harga').val();
            let bayar = $('#bayar').val();
            let kembalian =parseInt(bayar) - parseInt(total);
            $('#kembalian').val(kembalian);
        }

        $('#bayar').keyup(function () {
            hitungKembalian();
        })
    });
</script>
