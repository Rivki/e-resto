<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-3 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><?= $judul ?></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <select name="" id="menus" class="form-control"></select>
                    <br>
                    <br>
                    <input type="number" min="1" class="form-control" id="qty" placeholder="Jumlah Pesanan">
                    <br>
                    <a href="#" id="add-more" class="btn btn-primary float-right">Tambah Pesanan</a>
                </div>
            </div>
        </div>
        <div class="col-xl-9 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><?= $judul ?></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="<?= base_url() ?>order/simpan" id="form2" method="post">
                        <div class="table-responsive col-md-12">
                            <table id="" class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama Makanan</th>
                                    <th>Meja</th>
                                    <th>Harga</th>
                                    <th>qty</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            <br>
                        </div>
                        <div class="col-md-7 offset-sm-5">
                            <select name="meja_id" id="meja-id" class="form-control">
                                <?php
                                foreach ($meja as $m) {
                                    if($m->status === 'used'){
                                        echo '<option value="' . $m->id . '" disabled>' . $m->nama . '</option>';
                                    }else{
                                        echo '<option value="' . $m->id . '">' . $m->nama . '</option>';
                                    }
                                }
                                ?>
                            </select>
                            <br>
                            <div class="form-group row">
                                <label for="" class="col-sm-4 col-form-label">Total</label>
                                <div class="col-sm-8">
                                    <input type="number" min="1" name="total" id="total" class="form-control"
                                           placeholder="0"
                                           readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-4 col-form-label">Diskon</label>
                                <div class="col-sm-8">
                                    <input type="number" min="1" name="diskon" id="diskon" max="100"
                                           class="form-control"
                                           placeholder="0">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-sm-4 col-form-label">Total Harga</label>
                                <div class="col-sm-8">
                                    <input type="number" min="1" name="total_harga" id="total_harga"
                                           class="form-control"
                                           placeholder="0" readonly>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <input type="button" id="order-button" class="btn btn-primary float-right" value="Order">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        let rowcount = 0;
        let id = 0;
        let nama = "";
        let kode = "";
        let harga = 0;
        let total = 0;
        let qty = 0;
        let subTotal = 0;

        function addNewRow() {
            var newRow = "<tr>";
            newRow += "<td>" + (rowcount + 1) + "</td>";
            newRow += "<td class='cari-kode'>" + kode + "</td>";
            newRow += "<td>" + nama + "</td>";
            newRow += "<td>" + harga + "</td>";
            newRow += "<td class='cari-qty'>" + qty + "</td>";
            newRow += "<td class='cari-sub-total'>" + (harga * qty) + "</td>";
            newRow += "<td>";
            newRow += "<a href='#' class='delete-row btn btn-danger'>Hapus</a>";
            newRow += "</td>";
            newRow += "<input type='hidden' value='" + id + "' name='menu_trs[" + rowcount + "][menu_id]'>";
            newRow += "<input class='send-qty' type='hidden' value='" + qty + "' name='menu_trs[" + rowcount + "][qty]'>";
            newRow += "</tr>";

            $("table tbody").append(newRow);
            delRowButton();
            rowcount = $("table tbody tr").length;
        }

        $("#menus").select2({
            placeholder: "Pilih Menu",
            ajax: {
                url: window.base_url + 'ajax/getMenu',
                dataType: 'json',
                delay: 100,
                processResults: function (data) {
                    console.log(data);
                    return {
                        results: data
                    };
                }, cache: true
            }
        });

        $("#order-button").click(function () {
            $("#form2").submit();
        });

        $("#menus").on("select2:select", function (event) {
            var menu = event.params.data;
            id = menu.id;
            kode = menu.kode;
            nama = menu.nama;
            harga = menu.harga;
            if (menu.status ==='habis'){
                swal.fire("Warning", "Menu " + nama + " sudah habis", "info");
                // $('#add-more').attr('disabled', true);
            }
        });

        $("#add-more").click(function () {
            qty = $("#qty").val();
            subTotal = harga * parseInt(qty);
            let qtyList = 0;
            let newAddRow = true;
            let indexQty;
            $(".cari-kode").each(function (i, obj) {
                let kodeCari = $(this).html();
                if (kode === kodeCari) {
                    indexQty = i;
                    newAddRow = false;
                    qtyList += parseInt($(".cari-qty").eq(i).html());
                }
            });
            let qtyTotal = parseInt(qty) + parseInt(qtyList);
            if (newAddRow) {
                addNewRow()
            } else {
                $(".cari-qty").eq(indexQty).html(qtyTotal);
                $(".send-qty").eq(indexQty).val(qtyTotal);
                let xTotal = harga * qty;
                $(".cari-sub-total").eq(indexQty).html(xTotal);
            }
            total += subTotal;
            hitungTotal();
        });

        function hitungTotal() {
            $('#total').val(total);
            let diskon = $('#diskon').val();
            parsePercent = (diskon / 100) * parseInt(total);
            let grandTotal = parseInt(total) - parseInt(parsePercent);
            $("#total_harga").val(grandTotal);
        }

        $("#diskon").keyup(function () {
            hitungTotal();
        });

        function delRowButton() {
            $(".delete-row").click(function () {
                let tr = $(this).closest("tr");
                tr.remove();
                rowcount -= 1;
            });
        }
    })
</script>