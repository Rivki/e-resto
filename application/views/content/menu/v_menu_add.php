<div class="col-xl-12 order-xl-1">
    <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0"><?= $judul ?></h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Kode Makanan</label>
                                <input type="text" name="kode" id="id" class="form-control form-control-alternative" placeholder="Kode Makanan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Nama Makanan</label>
                                <input type="text" name="nama" class="form-control form-control-alternative" placeholder="Nama Makanan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Harga</label>
                                <input type="number" name="harga" min="1" class="form-control form-control-alternative" placeholder="Harga">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Jenis Makanan</label>
                                <select name="id_jenis" class="form-control form-control-alternative" id="">
                                    <?php
                                    foreach ($menus as $m){
                                        echo '<option value='.$m->id.'>'.$m->nama.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?= base_url() ?>menus" class="btn btn-danger">Kembali</a>
                <input type="submit" class="btn btn-success float-right" name="submit" value="Simpan">
            </form>
        </div>
    </div>
</div>