<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <h3 class="mb-0"><?= $judul ?></h3>
            </div>
            <div class="table-responsive">
                <table id="dataTable" class="table align-items-center table-flush">
                    <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th class="hidden">id</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Jenis Makanan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $no = 1;
                    foreach ($menus as $j) {
                        ?>
                        <form action="<?= base_url() ?>menus/changeStatus/<?= $j->id_menu ?>" method="post">
                            <tr>
                                <td><?= $no++ ?></td>
                                <td class="hidden id"><?= $j->id_menu ?></td>
                                <td><?= $j->nama_menu ?></td>
                                <td><?= rupiah($j->harga_menu) ?></td>
                                <td><?= $j->jenis_makanan ?></td>
                                <td><?= $j->status ?></td>
                                <td>
                                    <input type="submit" name="tersedia" class="btn btn-primary btn-sm" value="Tersedia">
                                    <input type="submit" name="habis" class="btn btn-danger btn-sm" value="Habis">
                                </td>
                            </tr>
                        </form>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(".hidden").hide();
    // $(function () {
    //     $("#btn-available").click(function () {
    //         let tr = $(this).closest("tr");
    //         let valuep = "tersedia";
    //         let id = tr.find(".id").html();
    //         let urlp = window.base_url + "menus/update/" + id;
    //         // alert(id);
    //         // alert(url);
    //         $.ajax({
    //             type: "POST",
    //             url: urlp,
    //             dataType: "json",
    //             data: {"status": valuep},
    //             success: function (data) {
    //                 console.log(data)
    //             },
    //             error: function (data) {
    //                 console.log(data)
    //             }
    //         });
    //     });
    // })
</script>
