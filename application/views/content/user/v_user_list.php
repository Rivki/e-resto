<div class="row">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <h3 class="mb-0"><?= $judul ?>
                    <a href="<?= base_url() ?>user/add" class="btn btn-primary btn-sm float-right">Tambah</a>
                </h3>
            </div>
            <div class="table-responsive">
                <table id="dataTable" class="table align-items-center table-flush">
                    <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th class="hidden" >id</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($user as $j) {
                        ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td class="hidden id"><?= $j->id ?></td>
                            <td><?= $j->nama ?></td>
                            <td><?= $j->jabatan ?></td>
                            <td>
                                <a href="<?= base_url() ?>user/update/<?= $j->id ?>"
                                   class="btn btn-circle btn-warning btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a href="#" class="btn btn-circle btn-sm btn-danger tombolHapus">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.hidden').hide();
        $(".tombolHapus").click(function () {
            let tr = $(this).closest("tr");
            id = tr.find(".id").html();
            console.log("id: " + id);
            Swal.fire({
                title: 'Are you sure?',
                text: "Apakah yakin menghapus data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        url: window.base_url + 'user/delete/' + id,
                        success: function (result) {
                            if (result) {
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                );
                                location.reload()
                            }
                        }
                    })
                }
            })
        });
    })
</script>
