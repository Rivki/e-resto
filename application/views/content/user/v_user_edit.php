<div class="col-xl-12 order-xl-1">
    <div class="card bg-secondary shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0"><?= $judul ?></h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post">
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Nama</label>
                                <input type="text" name="nama" id="input-username" value="<?= $user->nama ?>"
                                       class="form-control form-control-alternative" placeholder="Nama Makanan">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Password</label>
                                <input type="password" name="harga" min="1" id="input-username"
                                       class="form-control form-control-alternative" placeholder="Harga">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-username">Jabatan</label>
                                <select name="jabatan" class="form-control form-control-alternative" id="">
                                    <?php
                                    if ($user->jabatan == "manajer") {
                                        echo '<option value="manajer" selected>Manajer</option>';
                                        echo '<option value="kasir">Kasir</option>';
                                        echo '<option value="dapur">Dapur</option>';
                                    } elseif ($user->jabatan == "kasir") {
                                        echo '<option value="manajer">Manajer</option>';
                                        echo '<option value="kasir" selected>Kasir</option>';
                                        echo '<option value="dapur">Dapur</option>';
                                    } else {
                                        echo '<option value="manajer">Manajer</option>';
                                        echo '<option value="kasir">Kasir</option>';
                                        echo '<option value="dapur" selected>Dapur</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?= base_url() ?>user" class="btn btn-danger">Kembali</a>
                <input type="submit" class="btn btn-success float-right" name="submit" value="Simpan">
            </form>
        </div>
    </div>
</div>