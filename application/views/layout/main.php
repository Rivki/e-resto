<!DOCTYPE html>
<html>
<?php $this->load->view('layout/header.php') ?>
<body>
<!-- Sidenav -->
<?php $this->load->view('layout/sidebar.php') ?>
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <?php $this->load->view('layout/top_navbar.php') ?>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
<!--                --><?php //$this->load->view('layout/header_content.php') ?>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <?php $this->load->view($page) ?>
        <!-- Footer -->
        <?php $this->load->view('layout/main_footer.php') ?>
    </div>
</div>

<?php $this->load->view('layout/footer.php') ?>
</body>

</html>