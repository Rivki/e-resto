<!-- Argon Scripts -->
<!-- Core -->
<script src="<?= base_url() ?>/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<!-- Optional JS -->
<script src="<?php echo base_url(); ?>assets/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/swal/sweetalert.js"></script>
<script src="<?= base_url() ?>/assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="<?= base_url() ?>/assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->
<script src="<?= base_url() ?>/assets/js/argon.js?v=1.0.0"></script>
