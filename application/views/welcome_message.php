<div class="row" id="row-refresh">
    <div class="col">
        <div class="card shadow">
            <div class="card-header border-0">
                <h3 class="mb-0"><?= $judul ?>
                </h3>
            </div>
            <div class="table-responsive">
                <table id="dataTable" class="table align-items-center table-flush table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th class="hidden">id</th>
                        <th>Tanggal</th>
                        <th>Nama Meja</th>
                        <th>Status Order</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody class="refresh">
                    <?php
                    $no = 1;
                    foreach ($order as $j) {
                        if ($j->status_order == "paid") {
                            ?>
                            <tr class="xoxo">
                                <td><?= $no++ ?></td>
                                <td class="hidden id"><?= $j->id ?></td>
                                <td><?= $j->tanggal ?></td>
                                <?php
                                foreach ($meja as $m) {
                                    if ($j->meja_id == $m->id) {
                                        ?>
                                        <td><?= $m->nama ?></td>
                                        <?php
                                    }
                                }
                                ?>
                                <td><?= $j->status_order ?></td>
                            </tr>
                            </form>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span id="close-click" aria-hidden="true">×</span>
                </button>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center table-flush body-table">
                    <thead class="thead-light">
                    <tr>
                        <th>Nama Menu</th>
                        <th>Qty</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".hidden").hide();
        let j = 0;
        let rowLen = $(".refresh").length;
        let nama_menu = "";
        let qty = "";
        let tempId = "";
        let a = "";
        var hasil = [];

        $(".xoxo").click(function () {
            $(".body-table tbody").empty();
            let tr = $(this).closest("tr");
            id = tr.find(".id").html();
            let newRow = "";
            let addRow = true;
            let indexQty;
            // console.log("ID : "+id);
            $.ajax({
                url: window.base_url + "ajax/getDetailOrder",
                data: {
                    "q": id
                },
                type: 'get',

                success: function (result) {

                    hasil = JSON.parse(result);
                    // console.log(hasil);
                    for (let i = 0; i < hasil.length; i++) {
                        newRow = "<tr>";
                        newRow += "<td>" + hasil[i]["nama_menu"] + "</td>";
                        newRow += "<td>" + hasil[i]["qty"] + "</td>";
                        newRow += "</tr>";

                        // console.log(newRow);
                        $(".body-table tbody").append(newRow);
                    }


                    // $(".cari-kode").each(function (i, obj) {
                    //     let kodeCari = $(this).html();
                    //     if (tempId === kodeCari) {
                    //         indexQty = i;
                    //         addRow = false;
                    //     }
                    // });

                    // if (addRow) {
                    //     $("#body-model").append(newRow);
                    //     hasil.length = 0;
                    //     console.log(hasil.length = 0)
                    //
                    // } else {
                    //     console.log("data sudah ada")
                    // }

                },
                cache: true
            });
            $("#modalDetail").modal("show");
        });
    })

</script>
