<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0"><?= $judul ?></h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <div class="pl-lg-4">
                            <div class="row">
                                <!--===============================================================-->
                                <!--bagian untuk input pilih meja dan jumlah orang-->
                                <!--===============================================================-->
                                <div class="col-lg-6">
                                    <div class="form-group focused">
                                        <label for="num_people">Jumlah Pelanggan</label>
                                        <input type="number" name="num_people" id="num_people" class="form-control form-control-alternative" placeholder="Jumlah Pelanggan">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group focused">
                                        <label for="sel_meja">Pilih Meja</label>
                                        <select name="meja_id" id="" class="form-control form-control-alternative">
                                            <?php
                                            foreach ($meja as $item) {
                                                echo '<option value="'.$item->id.'">'.$item->nama.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!--===============================================================-->
                                <hr class="my-4"/>
                                <!--===============================================================-->
<!--                                bagian untuk list makanan-->
<!--                                ===============================================================-->
                                <div class="col-lg-12">
                                    <h6 class="heading-small text-muted mb-4">List Makanan</h6>
                                    <table class="table table-sm border-0 table-id">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th class="hidden">id</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($menu as $m) {
                                            if ($m->id_jenis == '1') {
                                                ?>
                                                <tr class="click-row">
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" value="item_trx[<?= $m->id ?>][menu_id]" class="custom-control-input"
                                                                   id="<?= $m->id ?>">
                                                            <label class="custom-control-label"
                                                                   for="<?= $m->id ?>"><?= $m->nama ?></label>
                                                        </div>
                                                    </td>
                                                    <td><?= rupiah($m->harga) ?></td>
                                                    <td>
                                                        <input type="number" min="1" class="form-control form-control-alternative">
                                                    </td>
                                                    <td class="hidden id"><?=$m->id?></td>
                                                </tr>

                                                <?php
                                            }
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>
<!--                                ===================================================================-->
                            </div>
                        </div>
                        <hr class="my-4"/>
                        <!--===============================================================-->
                        <!--bagian untuk list minuman-->
                        <!--===============================================================-->
                        <h6 class="heading-small text-muted mb-4">List Minuman</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-sm border-0">
                                        <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th class="hidden">id</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($menu as $m) {
                                            if ($m->id_jenis == '2') {
                                                ?>
                                                <tr class="clik-row">
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="" value="item_trx[<?= $m->id ?>][menu_id]" class="custom-control-input ck-click"
                                                                   id="<?= $m->id ?>">
                                                            <label class="custom-control-label"
                                                                   for="<?= $m->id ?>"><?= $m->nama ?></label>
                                                        </div>
                                                    </td>
                                                    <td><?= rupiah($m->harga) ?></td>
                                                    <td>
                                                        <input type="number" name="" value="item_trx[<?=$m->id?>][qty]" min="1" class="form-control form-control-alternative">
                                                    </td>
                                                    <td class="hidden id"></td>
                                                </tr>

                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--===============================================================-->
                        <div class="col-lg-12">
                            <div class="form-group focused">
                                <label for="num_people">Total Harga</label>
                                <input type="number" name="total_harga" id="total_harga" class="form-control form-control-alternative">
                            </div>
                        </div>
                        <input type="submit" class="btn btn-success float-right" value="Order">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".hidden").hide();
    $(function () {
        let subTotal = 0;
        $('.click-row').click(function () {
            let tr = $(this).closest("tr");
            idCheck = tr.find('.id').html();
            temp = [];
            if(idCheck === temp.values()){
                temp.wrap(idCheck);
                console.log(temp)
            }else {

            }
            // for (let i = 0; i <temp.length ; i++) {
            //
            // }
            // temp = [];
            // if (idCheck === temp.values()){
            //
            // }
            // console.log(idCheck);
        });

        // function getTotal() {
        //     if ($(idCheck).prop('checked') === true){
        //         alert("kamu memilih "+idCheck);
        //     }
        //     // harga = tr.find(".")
        // }
        // getTotal()
    })
</script>
