<?php

/**
 * @property  input
 * @property  db
 */

class Ajax extends CI_Controller
{
    public function checkMenuKode()
    {
        $id = $this->input->get("q");
        $produk = $this->db->where(array("id" => $id))->get("menu")->result();
        $json = array("data" => "" . sizeof($produk));
        echo json_encode($json);
    }

    public function getIdOrder()
    {
        $query = "select id from orders";
        $json = $this->db->query($query);
        echo json_encode($json);
    }

    public function getMenu()
    {
        $id = $this->input->get("q");
        $query = "select id, nama as text, nama, kode, harga, id_jenis, status ";
        $query .= "from menu where id like '%" . $id . "%' OR ";
        $query .= "nama like '%" . $id . "%'";
        $json = $this->db->query($query)->result();
//        var_dump($json);
        echo json_encode($json);
    }

    public function getDetailOrder()
    {
        $id = $this->input->get("q");
        $query = "select o.id, m.nama as nama_menu, om.qty as qty ";
        $query .= "from orders_menu om, menu m, orders o ";
        $query .= "where om.menu_id = m.id and om.orders_id = o.id and o.id like '%" . $id . "%'";
        $json = $this->db->query($query)->result();
        echo json_encode($json);
    }
}