<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-26
 * Time: 19:18
 */

class Menus extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array("MenusModel", "JenisModel"));
        if ($this->session->userdata('logged_in') !== true){
            redirect('login');
        }
    }

    public function index()
    {
        $data['page'] = "content/menu/v_menu_list.php";
        $data['judul'] = "Menus";
        $data['menus'] = $this->MenusModel->getAll()->result();
        $this->load->view('layout/main', $data);
    }

    public function indexKitchen()
    {
        $data['page'] = "content/menu/v_menu_list_for_kitchen.php";
        $data['judul'] = "Menus";
        $data['menus'] = $this->MenusModel->getAll()->result();
        $this->load->view('layout/main', $data);
    }

    public function add()
    {
        if ($this->input->post("submit")) {
            $data = array(
                "kode" => $this->input->post("kode"),
                "nama" => $this->input->post("nama"),
                "harga" => $this->input->post("harga"),
                'status' => 'tersedia',
                "id_jenis" => $this->input->post("id_jenis")
            );
            $this->MenusModel->insert($data);
            redirect('menus');
        } else {
            $data['menus'] = $this->JenisModel->getAll()->result();
            $data["page"] = "content/menu/v_menu_add";
            $data["judul"] = "Tambah Menus";
            $this->load->view('layout/main', $data);
        }

    }

    public function update($id)
    {
        if ($this->input->post("submit")) {
            $data = array(
                "kode" => $this->input->post("kode"),
                "nama" => $this->input->post("nama"),
                "harga" => $this->input->post("harga"),
                "id_jenis" => $this->input->post("id_jenis")
            );
            $this->MenusModel->update($id, $data);
            redirect('menus');
        } else {
            $data["page"] = "content/menu/v_menu_edit.php";
            $data["judul"] = "Ubah Data Menus";
            $m = $this->MenusModel->getWhere(array("id_menu" => $id))->result();
            $data['menus'] = $m[0];
            $data["jenis"] = $this->JenisModel->getAll()->result();
            $this->load->view('layout/main', $data);
        }

    }

    public function changeStatus($id)
    {
        if ($this->input->post("tersedia")){
            $data = array(
                "status" => "tersedia"
            );
        }elseif ($this->input->post("habis")){
            $data = array(
                "status" => "habis"
            );
        }
        $this->MenusModel->update($id, $data);
        redirect('menus/indexKitchen');
    }

    public function delete($id)
    {
        $this->MenusModel->delete($id);
        redirect("menus");
    }


}