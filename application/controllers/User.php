<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-26
 * Time: 23:22
 */

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("UserModel");
    }

    public function index()
    {
        $data['page'] = "content/user/v_user_list.php";
        $data['judul'] = "User";
        $data['user'] = $this->UserModel->getAll()->result();
        $this->load->view('layout/main', $data);
        if ($this->session->userdata('logged_in') !== true){
            redirect('login');
        }
    }

    public function add()
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama"),
                "password" => sha1($this->input->post("password")),
                "jabatan" => $this->input->post("jabatan")
            );
            $this->UserModel->insert($data);
            redirect('user');
        } else {
            $data['menus'] = $this->UserModel->getAll()->result();
            $data["page"] = "content/user/v_user_add";
            $data["judul"] = "Tambah User";
            $this->load->view('layout/main', $data);
        }
    }

    public function update($id)
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama"),
                "jabatan" => $this->input->post("jabatan")
            );
            $this->UserModel->update($id,$data);
            redirect('user');
        } else {
            $data["page"] = "content/user/v_user_edit";
            $m = $this->UserModel->getWhere(array("id" => $id))->result();
            $data['user'] = $m[0];
            $data["judul"] = "Tambah User";
            $this->load->view('layout/main', $data);
        }
    }

    public function delete($id)
    {
        $this->UserModel->delete($id);
        redirect("user");
    }
}