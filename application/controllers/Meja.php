<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-27
 * Time: 12:59
 */

class Meja extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("MejaModel");
        if ($this->session->userdata('logged_in') !== true){
            redirect('login');
        }
    }

    public function index()
    {
        $data['page'] = "content/meja/v_meja_list.php";
        $data['judul'] = "Meja";
        $data['meja'] = $this->MejaModel->getAll()->result();
        $this->load->view('layout/main', $data);
    }

    public function add()
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama"),
                "status" => $this->input->post("status")
            );
            $this->MejaModel->insert($data);
            redirect('meja');
        } else {
            $data['meja'] = $this->MejaModel->getAll()->result();
            $data["page"] = "content/meja/v_meja_add";
            $data["judul"] = "Tambah Meja";
            $this->load->view('layout/main', $data);
        }
    }

    public function update($id)
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama"),
                "status" => $this->input->post("status")
            );
            $this->MejaModel->update($id,$data);
            redirect('meja');
        } else {
            $data["page"] = "content/meja/v_meja_edit";
            $j = $this->MejaModel->getWhere(array("id" => $id))->result();
            $data['meja'] = $j[0];
            $data["judul"] = "Ubah Meja Meja";
            $this->load->view('layout/main', $data);
        }
    }

    public function delete($id)
    {
        $this->MejaModel->delete($id);
        redirect("meja");
    }
}