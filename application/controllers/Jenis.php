<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-26
 * Time: 22:37
 */

class Jenis extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("JenisModel");
        if ($this->session->userdata('logged_in') !== true){
            redirect('login');
        }
    }

    public function index()
    {
        $data['page'] = "content/jenis/v_jenis_list.php";
        $data['judul'] = "Jenis Makanan";
        $data['jenis'] = $this->JenisModel->getAll()->result();
        $this->load->view('layout/main', $data);
    }

    public function add()
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama")
            );
            $this->JenisModel->insert($data);
            redirect('jenis');
        } else {
            $data['menus'] = $this->JenisModel->getAll()->result();
            $data["page"] = "content/jenis/v_jenis_add";
            $data["judul"] = "Tambah Jenis Makanan";
            $this->load->view('layout/main', $data);
        }
    }

    public function update($id)
    {
        if ($this->input->post("submit")) {
            $data = array(
                "nama" => $this->input->post("nama")
            );
            $this->JenisModel->update($id,$data);
            redirect('jenis');
        } else {
            $data["page"] = "content/jenis/v_jenis_edit";
            $j = $this->JenisModel->getWhere(array("id" => $id))->result();
            $data['jenis'] = $j[0];
            $data["judul"] = "Edit Jenis Makanan";
            $this->load->view('layout/main', $data);
        }
    }

    public function delete($id)
    {
        $this->JenisModel->delete($id);
        redirect("jenis");
    }
}