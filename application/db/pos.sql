drop database pos_restoran;
create database pos_restoran;
use pos_restoran;

create table user(
    id int not null primary key auto_increment,
    nama varchar(50),
    password varchar(50),
    jabatan enum('manajer','kasir','dapur')
);

create table jenis(
    id int not null primary key auto_increment,
    nama char(30) unique
);

-- m2m dengan orders
create table menu( 
    id int not null primary key auto_increment,
    nama varchar(100) unique,
    harga double,
    id_jenis int not null,
    foreign key (id_jenis) references jenis(id)
);

create table meja(
    id int not null primary key auto_increment,
    nama varchar (50),
    status enum('free','used')
);

create table orders(
    id int not null primary key auto_increment,
    meja_id int not null,
    num_people int,
    tanggal date,
    total_harga double,
    status_order enum('cooking', 'delivery')
);

create table orders_menu(
    id int not null primary key auto_increment,
    orders_id int not null,
    menu_id int not null,
    foreign key (orders_id) references orders(id),
    foreign key (menu_id) references menu(id)
);

create or replace view get_jenis_menu as
select m.id as id_menu, 
m.nama as nama_menu,
m.harga as harga_menu,
m.status as status,
j.id as id_jenis,
j.nama as jenis_makanan
from menu m, jenis j
where m.id_jenis = j.id;

select o.id as id_order, m.id as id_meja, m.nama as nama_meja, o.diskon as diskon,
       o.total as total, o.total_harga as total_harga, o.status_order as status_order
from orders o, menu m
where o.meja_id = m.id;

select m.nama as nama_menu, om.qty as qty
from orders_menu om , menu m, orders o
where om.menu_id = m.id  and o.id like 11;

select m.nama as nama_menu, sum(om.qty) as qty
from orders_menu om , menu m, orders o
where om.menu_id = m.id order by om.menu_id;

create or replace view get_transaksi as
select om.id as id, o.id as id_orders, m.nama, om.qty, m.harga
from orders_menu om, menu m, orders o
where om.menu_id = m.id and om.orders_id = o.id;

# select * from orders where id="11";
select * from getTransaksi where id = "11";