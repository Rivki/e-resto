<?php


class OrderMenuModel extends CI_Model
{
    private $table = "orders_menu";

    public function getAll()
    {
        return $this->db->get($this->table);
    }

    public function getWhere($attributes)
    {
        return $this->db->where($attributes)->get($this->table);
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        return $this->db->where(array("id" => $id))->update($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where(array("id" => $id))->delete($this->table);
    }

    public function getDetailOrder($id)
    {
        $query = "select o.id as id, m.nama as nama_menu, om.qty as qty ";
        $query .= "from orders_menu om , menu m, orders o ";
        $query .= "where om.menu_id = m.id  and o.id like '%".$id."%'";
        return $this->db->query($query);
    }

}