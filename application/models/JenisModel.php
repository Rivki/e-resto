<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-26
 * Time: 21:14
 */

class JenisModel extends CI_Model
{
    private $table = "jenis";

    public function getAll()
    {
        return $this->db->get($this->table);
    }

    public function getWhere($attributes)
    {
        return $this->db->where($attributes)->get($this->table);
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        return $this->db->where(array("id" => $id))->update($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where(array("id" => $id))->delete($this->table);
    }
}