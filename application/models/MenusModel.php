<?php
/**
 * Created by PhpStorm.
 * User: rivki
 * Date: 2019-03-26
 * Time: 19:17
 */

class MenusModel extends CI_Model
{
    private $table = "menu";
    private $table2 = "get_jenis_menu";

    public function getAll()
    {
        return $this->db->get($this->table2);
    }

    public function getOnlyMenus()
    {
        return $this->db->get($this->table);
    }

    public function getWhere($attributes)
    {
        return $this->db->where($attributes)->get($this->table2);
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($id, $data)
    {
        return $this->db->where(array("id" => $id))->update($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where(array("id" => $id))->delete($this->table);
    }

}