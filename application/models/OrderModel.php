<?php


class OrderModel extends CI_Model
{
    private $table = "orders";
    private $table2 = "get_transaksi";

    public function getAll()
    {
        return $this->db->get($this->table);
    }

    public function getWhere($attributes)
    {
        return $this->db->where($attributes)->get($this->table);
    }

    public function getWhereTrx($attributes)
    {
        return $this->db->where($attributes)->get($this->table2);
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function insertGetId($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data)
    {
        return $this->db->where(array("id" => $id))->update($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where(array("id" => $id))->delete($this->table);
    }
    public function getAllWithTable()
    {
        $query = "select (select m.nama from meja m where o.meja_id = m.id) as nama_meja, ";
        $query .= "o.id as order_id, om.qty as qty, mn.nama as menu, o.status_order as status_order ";
        $query .= "from orders_menu om, menu mn, orders o ";
        $query .= "where om.orders_id = o.id and om.menu_id = mn.id;";
        return $this->db->query($query);
    }
    public function getTransaction($id)
    {
        $query = "select o.id, m.nama as nama_menu, om.qty as qty, m.harga";
        $query .= "from orders_menu om, menu m, orders o";
        $query .= "where om.menu_id = m.id and om.orders_id = o.id and o.id like '".$id."'";
        return $this->db->query($query);
    }
}