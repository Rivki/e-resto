<?php

function perikasLogin()
{
    if (!$this->session->userData("logged_in")) {
        redirect("login");
    }
}

function dd($data)
{
    highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
}

function rupiah($angka)
{
    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
}
